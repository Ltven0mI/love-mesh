# Love-Mesh
This library can be used to serialize / deserialize a [love2d](https://love2d.org/) mesh to and from the LMF (Love Mesh Format)

# Usage
Start by requiring:  
```lua
local lovemesh = require("love-mesh")
```

To serialize a mesh:  
```lua
local data = lovemesh.serialize(mesh)
```

To deserialize a mesh:  
```lua
local mesh = lovemesh.deserialize(data)
```

Example:
```lua
-- Require library
local lovemesh = require("love-mesh")

-- Create mesh to serialize
local vertices = {
  {-1, -1},
  {1, -1},
  {0, 1}
}
local mesh = love.graphics.newMesh(vertices)

-- Serialize mesh to string
local data = lovemesh.serialize(mesh)

--[[
  Perhaps do something with the data string.
  Like write to file for example.
]]

-- Deserialize string to mesh
local newMesh = lovemesh.deserialize(data)

-- Draw mesh to screen
function love.draw()
  love.graphics.draw(newMesh, 100, 100, 0, 50, 50)
end
```