local m = {} -- Module

local bit = require("bit")
local ffi = require("ffi")

m._headerString = "LMF"
m._version = {major=1, minor=0}

local function _float2int(float)
  local f = ffi.new("float[1]")
  f[0] = float
  return ffi.cast("int*", f)[0]
end

local function _int2float(int)
  local i = ffi.new("int[1]")
  i[0] = int
  return ffi.cast("float*", i)[0]
end

local function _serializeByte(byte, index, resultTable)
  resultTable[index] = string.char(byte)
  return index + 1
end

local function _serializeColorByte(byte, index, resultTable)
  resultTable[index] = string.char(byte * 255)
  return index + 1
end

local function _serializeNum16(num, index, resultTable)
  for i=0, 1 do
    local b = bit.band(bit.rshift(num, (1-i)*8), 255)
    resultTable[index + i] = string.char(b)
  end
  return index + 2
end

local function _serializeNum32(num, index, resultTable)
  local intNum = _float2int(num)
  for i=0, 3 do
    local b = bit.band(bit.rshift(intNum, (3-i)*8), 255)
    resultTable[index + i] = string.char(b)
  end
  return index + 4
end

local function _serializeString(str, index, resultTable)
  resultTable[index] = str
  return index + 1 -- Increment by 1 only because using a single entry
end

local function _serializeHeader(index, resultTable)
  -- Add LMF to resultTable
  index = _serializeString(m._headerString, index, resultTable)

  -- Add version to resultTable
  index = _serializeByte(m._version.major, index, resultTable)
  index = _serializeByte(m._version.minor, index, resultTable)
  
  return index
end

local function _serializeDrawMode(mesh, index, resultTable)
  -- Get draw mode
  local mode = mesh:getDrawMode()
  local modeByte = 0
  if mode == "fan" then
    modeByte = 1
  elseif mode == "strip" then
    modeByte = 2
  elseif mode == "triangles" then
    modeByte = 3
  elseif mode == "points" then
    modeByte = 4
  else
    error("Unknown mesh drawmode: "..tostring(mode))
  end

  -- Add modeByte to resultTable
  index = _serializeByte(modeByte, index, resultTable)
  
  return index
end

local function _serializeAttribute(attrib, index, resultTable)
  local name = attrib[1]
  local datatype = attrib[2]
  local components = attrib[3]

  -- Add length of name to resultTable
  index = _serializeByte(#name, index, resultTable)
  -- Add name to resultTable
  index = _serializeString(name, index, resultTable)

  -- Get the corresponding byte for the datatype
  local datatypeByte = 0
  if datatype == "byte" then
    datatypeByte = 1
  elseif datatype == "float" then
    datatypeByte = 2
  else
    error("Unknown mesh-vertexformat-attribute-datatype: "..datatype)
  end

  -- Add datatypeByte to resultTable
  index = _serializeByte(datatypeByte, index, resultTable)
  -- Add components (Component Count) to resultTable
  index = _serializeByte(components, index, resultTable)

  return index
end

local function _serializeFormat(mesh, index, resultTable)
  local format = mesh:getVertexFormat()

  -- Get number of attributes
  local attribCount = #format

  -- Add attribCount as the first entry to resultTable
  index = _serializeByte(attribCount, index, resultTable)

  -- Add attributes to resultTable
  for i=1, attribCount do
    index = _serializeAttribute(format[i], index, resultTable)
  end

  return index
end

local function _serializeVertex(vertex, format, index, resultTable)
  local vertOffset = 0

  -- Iterate over attributes in format
  for _, attrib in ipairs(format) do
    local datatype = attrib[2]
    local components = attrib[3]
    -- print(attrib[1], attrib[2], attrib[3])

    -- Get serializing function
    local func
    if datatype == "byte" then
      func = _serializeColorByte
    elseif datatype == "float" then
      func = _serializeNum32
    end

    -- Iterate over attrib components
    for i=1, components do
      index = func(vertex[i + vertOffset], index, resultTable)
      -- print("| "..vertex[i + vertOffset])
    end

    vertOffset = vertOffset + components
  end

  return index
end

local function _serializeVertices(mesh, index, resultTable)
  -- Get Vertex Count
  local vertCount = mesh:getVertexCount()

  -- Get Vertices
  local vertices = {}
  for i=1, vertCount do
    vertices[i] = {mesh:getVertex(i)}
    -- print(vertices[i][1])
  end

  -- Get Format
  local format = mesh:getVertexFormat()

  -- Add vertCount to resultTable
  index = _serializeNum16(vertCount, index, resultTable)

  -- Serialize Vertices
  for i=1, vertCount do
    index = _serializeVertex(vertices[i], format, index, resultTable)
  end

  return index
end

local function _serializeIndices(mesh, index, resultTable)
  -- Get Vertex Map
  local vertexMap = mesh:getVertexMap()

  -- Handle no vertexMap
  if vertexMap == nil then
    index = _serializeNum32(0, index, resultTable)
    return index
  end

  -- Add index count to resultTable
  index = _serializeNum32(#vertexMap, index, resultTable)

  -- Add indices to resultTable
  for k, id in ipairs(vertexMap) do
    index = _serializeNum16(id, index, resultTable)
  end

  return index
end

local function _serialize(mesh)
  local resultTable = {}
  local index = 1

  index = _serializeHeader(index, resultTable)
  index = _serializeDrawMode(mesh, index, resultTable)
  index = _serializeFormat(mesh, index, resultTable)
  index = _serializeVertices(mesh, index, resultTable)
  index = _serializeIndices(mesh, index, resultTable)

  return table.concat(resultTable)
end

-- Deserialization

local function _deserializeByte(index, data)
  return index + 1, string.byte(data[index])
end

local function _deserializeColorByte(index, data)
  return index + 1, string.byte(data[index]) / 255
end

local function _deserializeNum16(index, data)
  local num = 0
  for i=0, 1 do
    local byteDecimal = string.byte(data[index + i])
    num = num + bit.lshift(byteDecimal, (1-i)*8)
  end
  return index + 2, num
end

local function _deserializeNum32(index, data)
  local intNum = 0
  for i=0, 3 do
    local byteDecimal = string.byte(data[index + i])
    intNum = intNum + bit.lshift(byteDecimal, (3-i)*8)
  end

  local num = _int2float(intNum)
  
  return index + 4, num
end

local function _deserializeString(length, index, data)
  local charTable = {}
  for i=1, length do
    charTable[i] = data[index]
    index = index + 1
  end
  return index, table.concat(charTable)
end

local function _deserializeHeader(index, data)
  -- Get and validate headerString
  local index, headerString = _deserializeString(3, index, data)
  if headerString ~= m._headerString then
    error("Invalid headerString: '"..tostring().."' Expected: '"..m._headerString.."'")
  end

  -- Get version
  local vMajor, vMinor = 0, 0
  local index, vMajor = _deserializeByte(index, data)
  local index, vMinor = _deserializeByte(index, data)

  -- Validate version
  if vMajor ~= m._version.major or vMinor ~= m._version.minor then
    error(string.format("Version mix-match: '%s.%s' Expected: '%s.%s'", vMajor, vMinor, m._version.major, m._version.minor))
  end
  -- print(headerString, vMajor, vMinor)

  return index
end

local function _deserializeDrawmode(index, data)
  -- Get drawmodeByte
  local index, drawmodeByte = _deserializeByte(index, data)

  -- Get drawmode from drawmodeByte
  local drawmode
  if drawmodeByte == 1 then
    drawmode = "fan"
  elseif drawmodeByte == 2 then
    drawmode = "strip"
  elseif drawmodeByte == 3 then
    drawmode = "triangles"
  elseif drawmodeByte == 4 then
    drawmode = "points"
  else
    error("Unknown DrawModeByte: '"..tostring(drawmodeByte).."'")
  end

  return index, drawmode
end

local function _deserializeAttribute(index, data)
  -- Get name length
  local index, nameLength = _deserializeByte(index, data)

  -- Get name
  local index, name = _deserializeString(nameLength, index, data)

  -- Get datatypeByte
  local index, datatypeByte = _deserializeByte(index, data)

  -- Get datatype from datatypeByte
  local datatype = ""
  if datatypeByte == 1 then
    datatype = "byte"
  elseif datatypeByte == 2 then
    datatype = "float"
  else
    error("Unknown datatype byte: "..datatypeByte)
  end

  -- Get component count
  local index, components = _deserializeByte(index, data)

  return index, {name, datatype, components}
end

local function _deserializeFormat(index, data)
  -- Get attribute count
  local attribCount = 0
  index, attribCount = _deserializeByte(index, data)

  -- Deserialize Attributes
  local format = {}
  for i=1, attribCount do
    index, format[i] = _deserializeAttribute(index, data)
  end

  return index, format
end

local function _deserializeVertex(format, index, data)
  local vertex = {}
  local vertOffset = 0

  -- Iterate over attributes in format
  for _, attrib in ipairs(format) do
    local datatype = attrib[2]
    local components = attrib[3]

    -- Get deserializing function
    local func
    if datatype == "byte" then
      func = _deserializeColorByte
    elseif datatype == "float" then
      func = _deserializeNum32
    end

    -- Iterate over attrib components
    for i=1, components do
      index, vertex[i + vertOffset] = func(index, data)
      -- print("COMPONENT: "..vertex[i + vertOffset])
    end
    vertOffset = vertOffset + components
  end

  return index, vertex
end

local function _deserializeVertices(format, index, data)
  -- Get vertex count
  local index, vertCount = _deserializeNum16(index, data)
  -- print("vertcount: "..vertCount)

  -- Deserialize vertices
  local vertices = {}
  for i=1, vertCount do
    -- print("Vert : "..i)
    index, vertices[i] = _deserializeVertex(format, index, data)
    -- print(unpack(vertices[i]))
  end

  return index, vertices
end

local function _deserializeIndices(index, data)
  local index, indexCount = _deserializeNum32(index, data)

  -- print("Index Count = " .. indexCount)

  -- Handle indexCount of 0
  if indexCount == 0 then
    return index, nil
  end

  local indices = {}

  -- Deserialize indices
  for i=1, indexCount do
    index, indices[i] = _deserializeNum16(index, data)
  end
  return index, indices
end

local function _deserialize(data)
  local index = 1

  index = _deserializeHeader(index, data)
  local index, drawmode = _deserializeDrawmode(index, data)
  local index, format = _deserializeFormat(index, data)
  local index, vertices = _deserializeVertices(format, index, data)
  local index, indices = _deserializeIndices(index, data)

  local mesh = love.graphics.newMesh(format, vertices, drawmode, "dynamic")

  mesh:setVertexMap(indices)
  
  return mesh
end


function m.serialize(mesh)
  return _serialize(mesh)
end

function m.deserialize(strData)
  local tbl = {}
  local index = 1
  strData:gsub(".", function(n) tbl[index] = n; index = index + 1 end)
  return _deserialize(tbl)
end

return m